import sys
import sdl2
import sdl2.ext
import config
import draw
import math
import ctypes
import numpy as np


def run():
    sdl2.ext.init()

    window = create_window()
    window.show()
    renderer = Renderer(window,
                        config.WINDOW_WIDTH,
                        config.WINDOW_HEIGHT,
                        config.WINDOW_WIDTH / 2,
                        config.WINDOW_HEIGHT / 2)

    fi = -math.pi / 6

    projection_matrix = [Vector([1, 0, 0, 0]),
                         Vector([0, 1, 0, 0]),
                         Vector([-math.cos(fi) * 0.5, math.sin(fi) * 0.5, 1, 0]),
                         Vector([0, 0, 0, 1])]

    size = 100
    polygons = []
    polygons.extend(create_cube(size, -size, -size, -size))
    polygons.extend(create_cube(size, 0, -size, -size))
    polygons.extend(create_cube(size, 0, -size, 0))
    polygons.extend(create_cube(size, 0, 0, -size))

    axis_length = 600
    axis_color = sdl2.ext.Color(0, 0, 0, 255)    
    x_axis_vector = Vector([axis_length, 0, 0, 1])
    y_axis_vector = Vector([0, axis_length, 0, 1])
    z_axis_vector = Vector([0, 0, axis_length, 1])
    projected_axis = mul_matrix([x_axis_vector, 
                                 y_axis_vector, 
                                 z_axis_vector], 
                                projection_matrix)

    projected_polygons = []
    for polygon in polygons:
        projected_polygons.append(Polygon(mul_matrix(polygon.points, projection_matrix)))

    running = True
    i = 0
    pressed = False
    while running:
        events = sdl2.ext.get_events()
        for event in events:
            if event.type == sdl2.SDL_QUIT:
                running = False
                break
            if event.type == sdl2.SDL_WINDOWEVENT:
                if event.window.event == sdl2.SDL_WINDOWEVENT_RESIZED:
                    renderer.update_scale(event.window.data1,
                                          event.window.data2)
            if event.type == sdl2.SDL_MOUSEBUTTONDOWN:
                x, y = ctypes.c_int(0), ctypes.c_int(0)
                buttonstate = sdl2.mouse.SDL_GetMouseState(ctypes.byref(x), ctypes.byref(y))
                pressed = True
                prev_x = x.value - config.WINDOW_WIDTH / 2
                prev_y = config.WINDOW_HEIGHT - y.value - config.WINDOW_HEIGHT / 2
                axis = get_axis(prev_x, prev_y)
            if event.type == sdl2.SDL_MOUSEBUTTONUP:
                pressed = False
            if event.type == sdl2.SDL_MOUSEMOTION:
                if pressed:
                    x, y = ctypes.c_int(0), ctypes.c_int(0)
                    buttonstate = sdl2.mouse.SDL_GetMouseState(ctypes.byref(x), ctypes.byref(y))
                    current_x = x.value - config.WINDOW_WIDTH / 2
                    current_y = config.WINDOW_HEIGHT - y.value - config.WINDOW_HEIGHT / 2
                    offset_x = current_x - prev_x
                    offset_y = current_y - prev_y
                    prev_x, prev_y = current_x, current_y
                    if axis == 1:
                        rotation_matrix = get_x_rotation_matrix(float(offset_y * 7) / config.WINDOW_WIDTH)
                    elif axis == 2:
                        rotation_matrix = get_y_rotation_matrix(float(offset_x * 7) / config.WINDOW_HEIGHT)
                    elif axis == 3:
                        rotation_matrix = get_z_rotation_matrix(math.copysign(7, offset_x) * float(math.sqrt(math.pow(offset_x, 2) + math.pow(offset_y, 2))) / math.sqrt(math.pow(config.WINDOW_WIDTH, 2) + math.pow(config.WINDOW_HEIGHT, 2)))
                    else:
                        rotation_matrix = None
                    
                    if rotation_matrix is not None:
                        for polygon in polygons:
                            polygon.points = mul_matrix(polygon.points, rotation_matrix)

        renderer.clear()

        draw_axis(renderer, projected_axis, axis_color)

        i = 0
        while i < len(polygons):
            projected_polygons[i].points = mul_matrix(polygons[i].points,
                                                      projection_matrix)
            i = i + 1

        differentiation_period_amount = 15.

        points = get_line_points(projected_polygons, differentiation_period_amount)
        z_map = get_z_map(projected_polygons, points)
        draw_lines_by_points(projected_polygons, differentiation_period_amount, renderer, z_map)

        renderer.refresh()

        sdl2.SDL_Delay(100)

    sdl2.ext.quit()

    return 0


def draw_lines_by_points(projected_polygons, amount, renderer, z_map):
    for polygon in projected_polygons:
        i = 0
        while i < len(polygon.points) - 1:
            start_x = polygon.points[i].coords[0]
            start_y = polygon.points[i].coords[1]
            start_z = polygon.points[i].coords[2]

            end_x = polygon.points[i + 1].coords[0]
            end_y = polygon.points[i + 1].coords[1]
            end_z = polygon.points[i + 1].coords[2]

            delta_x = (end_x - start_x) / amount
            delta_y = (end_y - start_y) / amount
            delta_z = (end_z - start_z) / amount

            temp_x = start_x
            temp_y = start_y
            temp_z = start_z

            j = 0
            not_drawn_counter = 0
            while j < int(amount):
                x = int(round(temp_x))
                y = int(round(temp_y))

                max_z = z_map.get(str(x) + "~" + str(y), -999999)

                if abs(temp_z - max_z) < 5 or temp_z > max_z:
                    renderer.draw_line(temp_x, temp_y, temp_x + delta_x, temp_y + delta_y,
                                       sdl2.ext.Color(255, 0, 0, 128))
                else:
                    if not_drawn_counter > 1:
                        renderer.draw_line(temp_x, temp_y, temp_x + delta_x, temp_y + delta_y,
                                           sdl2.ext.Color(255, 0, 0, 128))
                        not_drawn_counter = 0
                    else:
                        not_drawn_counter = not_drawn_counter + 1

                temp_x = temp_x + delta_x
                temp_y = temp_y + delta_y
                temp_z = temp_z + delta_z

                j = j + 1

            i = i + 1


def get_z_map(projected_polygons, points):
    z_map = {}
    for polygon in projected_polygons:
        start_point = polygon.points[0]
        outer_end_point = polygon.points[1]
        inner_end_point = polygon.points[len(polygon.points) - 2]

        outer_start_x = start_point.coords[0]
        outer_start_y = start_point.coords[1]
        outer_start_z = start_point.coords[2]
        inner_start_x = start_point.coords[0]
        inner_start_y = start_point.coords[1]
        inner_start_z = start_point.coords[2]

        outer_end_x = outer_end_point.coords[0]
        outer_end_y = outer_end_point.coords[1]
        outer_end_z = outer_end_point.coords[2]
        inner_end_x = inner_end_point.coords[0]
        inner_end_y = inner_end_point.coords[1]
        inner_end_z = inner_end_point.coords[2]

        outer_delta_x = (outer_end_x - outer_start_x) / 100.
        outer_delta_y = (outer_end_y - outer_start_y) / 100.
        outer_delta_z = (outer_end_z - outer_start_z) / 100.

        inner_delta_x = (inner_end_x - inner_start_x) / 100.
        inner_delta_y = (inner_end_y - inner_start_y) / 100.
        inner_delta_z = (inner_end_z - inner_start_z) / 100.

        for point in points:
            coords = point.split("~")
            x = int(coords[0])
            y = int(coords[1])

            a = np.array([[outer_delta_x, inner_delta_x], [outer_delta_y, inner_delta_y]])
            b = np.array([x - inner_start_x, y - inner_start_y])

            result = np.linalg.solve(a, b)

            if len(result) == 2 and result[0] > 0 and result[1] > 0 and result[0] <= 100 and result[1] <= 100:
                z = inner_start_z + math.floor(result[0]) * outer_delta_z + math.floor(result[1]) * inner_delta_z
                z_map[point] = max(z_map.get(point, -9999999), z)

    return z_map


def get_line_points(projected_polygons, amount):
    points = set([])
    for polygon in projected_polygons:
        i = 0
        while i < len(polygon.points) - 1:
            start_x = polygon.points[i].coords[0]
            start_y = polygon.points[i].coords[1]
            start_z = polygon.points[i].coords[2]

            end_x = polygon.points[i + 1].coords[0]
            end_y = polygon.points[i + 1].coords[1]
            end_z = polygon.points[i + 1].coords[2]

            delta_x = (end_x - start_x) / amount
            delta_y = (end_y - start_y) / amount
            delta_z = (end_z - start_z) / amount

            temp_x = start_x
            temp_y = start_y
            temp_z = start_z

            j = 0
            while j < int(amount):
                x = int(round(temp_x))
                y = int(round(temp_y))

                points.add(str(x) + "~" + str(y))

                temp_x = temp_x + delta_x
                temp_y = temp_y + delta_y
                temp_z = temp_z + delta_z

                j = j + 1

            i = i + 1
    return points


class Key:
    def __init__(self, x, y):
        self.x = x
        self.y = y


class PlaneEquation:
    def __init__(self, a, b, c, d):
        self.a = a
        self.b = b
        self.c = c
        self.d = d


def get_z_rotation_matrix(delta):
    return [Vector([math.cos(delta), math.sin(delta), 0, 0]),
            Vector([-math.sin(delta), math.cos(delta), 0, 0]),
            Vector([0, 0, 1, 0]),
            Vector([0, 0, 0, 1])]


def get_x_rotation_matrix(delta):
    return [Vector([1, 0, 0, 0]),
            Vector([0, math.cos(delta), math.sin(delta), 0]),
            Vector([0, -math.sin(delta), math.cos(delta), 0]),
            Vector([0, 0, 0, 1])]


def get_y_rotation_matrix(delta):
    return [Vector([math.cos(delta), 0, -math.sin(delta), 0]),
            Vector([0, 1, 0, 0]),
            Vector([math.sin(delta), 0, math.cos(delta), 0]),
            Vector([0, 0, 0, 1])]


def create_cube(size, x_offset, y_offset, z_offset):
    pol_1 = Polygon([Vector([0, 0, size, 1], 1),
                     Vector([size, 0, size, 1], 2),
                     Vector([size, 0, 0, 1],3),
                     Vector([0, 0, 0, 1], 4),
                     Vector([0, 0, size, 1], 1)])

    pol_2 = Polygon([Vector([size, 0, size, 1], 1),
                     Vector([size, 0, 0, 1], 2),
                     Vector([size, size, 0, 1],3),
                     Vector([size, size, size, 1], 4),
                     Vector([size, 0, size, 1], 1)])

    pol_3 = Polygon([Vector([0, 0, 0, 1], 1),
                     Vector([size, 0, 0, 1], 2),
                     Vector([size, size, 0, 1],3),
                     Vector([0, size, 0, 1], 4),
                     Vector([0, 0, 0, 1], 1)])

    pol_4 = Polygon([Vector([0, 0, 0, 1], 1),
                     Vector([0, 0, size, 1], 2),
                     Vector([0, size, size, 1],3),
                     Vector([0, size, 0, 1], 4),
                     Vector([0, 0, 0, 1], 1)])

    pol_5 = Polygon([Vector([0, 0, size, 1], 1),
                     Vector([size, 0, size, 1], 2),
                     Vector([size, size, size, 1],3),
                     Vector([0, size, size, 1], 4),
                     Vector([0, 0, size, 1], 1)])

    pol_6 = Polygon([Vector([0, size, size, 1], 1),
                     Vector([size, size, size, 1], 2),
                     Vector([size, size, 0, 1],3),
                     Vector([0, size, 0, 1], 4),
                     Vector([0, size, size, 1], 1)])

    return add_offset_to_polygons([pol_1, pol_2, pol_3, pol_4, pol_5, pol_6], x_offset, y_offset, z_offset)


def add_offset_to_polygons(polygons, offset_x, offset_y, offset_z):
    for polygon in polygons:
        i = 0
        while i < len(polygon.points):
            polygon.points[i].coords[0] = polygon.points[i].coords[0] + offset_x
            polygon.points[i].coords[1] = polygon.points[i].coords[1] + offset_y
            polygon.points[i].coords[2] = polygon.points[i].coords[2] + offset_z

            i = i + 1

    return polygons


def draw_axis(renderer, axis, color):
    i = 0
    while i < len(axis):
        renderer.draw_line(-axis[i].coords[0], 
                           -axis[i].coords[1], 
                           axis[i].coords[0],
                           axis[i].coords[1], color)
        i = i + 1


def get_axis(x, y):      
    angle_first = math.atan(float(y) / x)
    if x > 0 and y > 0:
        if angle_first < math.pi / 9:
            return 1
        elif angle_first > math.pi / 2 - math.pi / 9:
            return 2
        elif math.pi / 3 - math.pi / 9 < angle_first < math.pi / 3 + math.pi / 9:
            return 3
        else:
            return 0
    elif x < 0 < y:
        if angle_first < -(math.pi / 2 - math.pi / 9):
            return 2
        elif angle_first  > -math.pi / 9:
            return 1
        else:
            return 0
    elif x < 0 and y < 0:
        if angle_first < math.pi / 9:
            return 1
        elif angle_first > math.pi / 2 - math.pi / 9:
            return 2
        elif math.pi / 3 - math.pi / 9 < angle_first < math.pi / 3 + math.pi / 9:
            return 3
        else:
            return 0
    elif x > 0 > y:
        if angle_first < -(math.pi / 2 - math.pi / 9):
            return 2
        elif angle_first  > -math.pi / 9:
            return 1
        else:
            return 0
    else:
        return 0


def create_window():
    return sdl2.ext.Window(config.WINDOW_TITLE, size=(config.WINDOW_WIDTH,
                                                      config.WINDOW_HEIGHT),
                           flags=sdl2.SDL_WINDOW_RESIZABLE)


def mul_matrix(first_vectors, second_vectors):
    result = []    
    i = 0
    while i < len(first_vectors):
        j = 0
        result_vector = Vector([0, 0, 0, 0])        
        while j < len(second_vectors[0].coords):
            k = 0
            sum = 0
            while k < len(first_vectors[0].coords):
                sum = sum + (first_vectors[i].coords[k] * second_vectors[k].coords[j])
                k = k + 1
            result_vector.coords[j] = sum

            j = j + 1
        result.append(result_vector)       

        z_i = 0
        while z_i < len(result):
            z_i = z_i + 1    
        i = i + 1

    i = 0
    while i < len(result):
        i = i + 1

    return result


class Polygon:
    def __init__(self, points=[]):
        self.points = points


class Vector:
    def __init__(self, coords, index = -1):
        self.coords = coords
        self.index = index


class Renderer:
    CLEAR_COLOR = sdl2.ext.Color(255, 255, 255, 0)

    def __init__(self, window, native_width, native_height, left_offset, right_offset):
        self.renderer = sdl2.ext.Renderer(window)

        self.native_width = native_width
        self.native_height = native_height

        self.left_offset = left_offset
        self.top_offset = right_offset

        self.x_scale = 1
        self.y_scale = 1

        self.update_scale(native_width, native_height)

    def update_scale(self, width, height):
        self.x_scale = width / float(self.native_width)
        self.y_scale = height / float(self.native_height)

    def clear(self):
        self.renderer.clear(Renderer.CLEAR_COLOR)

    def draw_line(self, x1, y1, x2, y2, color):
        self.renderer.draw_line(points=(int(self.x_scale * (x1 + self.left_offset)),
                                        int(self.y_scale * (-y1 + self.top_offset)),
                                        int(self.x_scale * (x2 + self.left_offset)),
                                        int(self.y_scale * (-y2 + self.top_offset))),
                                color=color)

    def refresh(self):
        self.renderer.present()


if __name__ == "__main__":
    sys.exit(run())
