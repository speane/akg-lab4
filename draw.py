import sdl2.ext
import math

print -100 * math.pi / 4


class Drawer:
    LINE_COLOR = sdl2.ext.Color(0, 0, 0, 0)
    AXIS_COLOR = sdl2.ext.Color(14, 123, 52, 0)
    AXIS_LENGTH = 200
    AXIS = [[AXIS_LENGTH, 0],
            [0, AXIS_LENGTH],
            [-AXIS_LENGTH * math.sin(math.pi / 4),
             -AXIS_LENGTH * math.sin(math.pi / 4)]]

    def __init__(self, renderer):
        self.renderer = renderer

        self.points = [[100, 100, 100], [100, ]]

    def draw(self):
        self.draw_axis()
        self.renderer.draw_line(0, 0, 100, 100, Drawer.LINE_COLOR)

    def draw_axis(self):
        self.renderer.draw_line(0,
                                0,
                                Drawer.AXIS[0][0],
                                Drawer.AXIS[0][1],
                                Drawer.AXIS_COLOR)
        self.renderer.draw_line(0,
                                0,
                                Drawer.AXIS[1][0],
                                Drawer.AXIS[1][1],
                                Drawer.AXIS_COLOR)
        self.renderer.draw_line(0,
                                0,
                                Drawer.AXIS[2][0],
                                Drawer.AXIS[2][1],
                                Drawer.AXIS_COLOR)

